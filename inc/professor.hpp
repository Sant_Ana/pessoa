#ifndef PROFESSOR_H
#define PROFESSOR_H

#include <iostream>
#include <string>
#include "pessoa.hpp"
using namespace std;

class Professor : public Pessoa
{
public:
	Professor();
	void setMatricula(int matricula_in);
	void setDisciplinas(string disciplinas_in);
	void setProjeto(string projetos_in);
	void setSalario(double salario_in);
	int getMatricula();
	double getSalario();
	string getDisciplinas();
	string getProjeto();
private:
	int matricula_func;
	string disciplinas,projetos,nome;
	double salario;
};
#endif
