#ifndef ALUNO_H
#define ALUNO_H

#include <iostream>
#include <string>
#include "pessoa.hpp"

class Aluno : public Pessoa
{
private:
	string materia[6];
	int matricula;
	double ira;
	
public:
	Aluno();
	Aluno(string m1,string m2,string m3,string m4,string m5,string m6,string nome,string idade,string telefone,double ira_in,int matricula_in);
	void setMatricula(int matricula_in);
	int getMatricula();
	void setIra(double ira_in);
	double getIra();
	void setMateria();
	void getMateria();
	
	
};
#endif