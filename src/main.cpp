#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

using namespace std;

int main(int argc, char ** argv) {
	Pessoa aluno_1;
	Pessoa * aluno_2;
	Pessoa * aluno_3;
	

	/*Aluno aluno;

	aluno.setMateria();
	aluno.getMateria();*/

	
	aluno_1.setNome("Bruno");
	aluno_1.setTelefone("555-4444");
	aluno_1.setIdade("14");

	aluno_2 = new Pessoa(); 

	aluno_2->setNome("Maria");
	aluno_2->setTelefone("333-5555");
	aluno_2->setIdade("54");

	aluno_3 = new Pessoa("Joao","35","222-5555");

	cout << "Nome\tIdade\tTelefone" << endl;
	cout << aluno_1.getNome() << "\t" << aluno_1.getIdade() << "\t" << aluno_1.getTelefone() << endl;
	cout << aluno_2->getNome() << "\t" << aluno_2->getIdade() << "\t" << aluno_2->getTelefone() << endl;
	cout << aluno_3->getNome() << "\t" << aluno_3->getIdade() << "\t" << aluno_3->getTelefone() << endl;

	//Professor

	Professor professor_1;

	professor_1.setNome("Edson");
	professor_1.setDisciplinas("Algebra");
	professor_1.setTelefone("3333-3333");
	professor_1.setIdade("41");
	professor_1.setSalario(1200.00);
	professor_1.setProjeto("...");
	professor_1.setMatricula(12354947);

	cout << "\t --- Professor ---\t\n";
	cout << "Nome\tIdade\tTelefone\tDisciplinas\tSalario\tProjeto\tMatricula" << endl;
	cout << professor_1.getNome() << "\t" << professor_1.getIdade() << "\t" << professor_1.getTelefone() << "\t" << professor_1.getDisciplinas() << " \t " << professor_1.getSalario() << "\t" << professor_1.getProjeto() << "\t" << professor_1.getMatricula() << "\n" << endl;

	//Aluno

	Aluno * aluno;
	aluno = new Aluno("Algebra","Calculo Numérico","Orientação a Objetos","Técnicas de Programação","Teoria dos Gráfos","C1","João","20","1234-5678",5.0,123456);

	cout << "\t --- Aluno ---\t\n";
	cout << "Nome\tIdade\tTelefone\tMatricula\tIRA" << endl;
	cout << aluno->getNome() << "\t" << aluno->getIdade() << " \t " << aluno->getTelefone() << " \t " << aluno->getMatricula() << " \t " << aluno->getIra() << endl;
	cout << "\t --- Materias do objeto aluno ---\t\n";
	aluno->getMateria();



	delete(aluno_2);
	delete(aluno_3);

}
