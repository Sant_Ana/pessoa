#include "professor.hpp"
#include <string>

using namespace std;

Professor::Professor()
{
	nome = "xxx";
	matricula_func = 00;
	salario = 0.0;
}

void Professor::setMatricula(int matricula_in)
{
	matricula_func = matricula_in;
}
int Professor::getMatricula()
{
	return matricula_func;
}

void Professor::setDisciplinas(string disciplinas_in)
{
	disciplinas = disciplinas_in;
}
string Professor::getDisciplinas()
{
	return disciplinas;
}
void Professor::setProjeto(string projetos_in)
{
	projetos = projetos_in;
}
string Professor::getProjeto()
{
	return projetos;
}
void Professor::setSalario(double salario_in)
{
	salario = salario_in;
}
double Professor::getSalario()
{
	 return salario;
}
